# mqtt_rc_transmitter

## What is this?

A project to use a gamepad in a browser to send mqtt messages to an mqtt broker, which are picked up by an esp8266 which turns them into a PPM pulse train suitable for connecting to a standard R/C transmitter module or R/C transmitter trainer port, so that you can drive R/C things over the internet (like robots).

![Diagram](docs/diagram.png)

The project has 2 parts:

* gamepad-to-mqtt interface that runs in a browser (see docs/index.html)
* mqtt-to-rc-transmitter that runs on an esp8266 microcontroller connected up to a R/C TX module or R/C TX trainer port

It requires:

* An esp8266 board (wemos D1 Mini is my goto)
* An R/C transmitter module (e.g. FrSky XJT) or an R/C transmitter with trainer port
* An mqtt server (mosquitto is tiny and good) needs to have both websockets and mqtt ports available (Web page communicates via websockets, esp8266 uses regular mqtt port)
* Some R/C vehicle with which the transmitter module is already paired

## How to use

* Put the firmware on an esp8266
* Connect the PPM_PIN (D6 on a wemos board by default) to the PPM in on an R/C module
* Configure the esp8266 via WifiManager (built in access point) with wifi details and mqtt details
* Fire up the web page (docs/index.html)
  - Connect a game controller 
  - Select it from the list
  - Verify the numbers under "Axis Info" change
  - Put in the same mqtt details as the esp (don't worry about topic at the moment, it doesn't do anything at the moment)

