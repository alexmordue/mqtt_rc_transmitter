#include <FS.h>   
#include <Arduino.h>

#include <ESP8266WiFi.h>          //ESP8266 Core WiFi Library (you most likely already have this in your sketch)
#include <DNSServer.h>            //Local DNS Server used for redirecting all requests to the configuration portal
#include <ESP8266WebServer.h>     //Local WebServer used to serve the configuration portal
#include <WiFiManager.h>          //https://github.com/tzapu/WiFiManager WiFi Configuration Magic
#include <ArduinoJson.h>          //https://github.com/bblanchon/ArduinoJson (v5 maximum, v6 removes things this depends on)
#include <PubSubClient.h>


#define NUM_CHANNELS 8
#define CENTRE_POSITION 1500
#define PPM_CHANNEL_SEPARATOR_US 350
#define PPM_RESET_PULSE_US 8000
#define FAILSAFE_TIME_MS 500

#define PPM_PIN D6

unsigned long channel_output[NUM_CHANNELS+1] = {1500,1500,1500,1500,1500,1500,1500,1500,PPM_RESET_PULSE_US};
unsigned long failsafe_output[NUM_CHANNELS+1] = {1500,1500,1500,1500,1500,1500,1500,1500,PPM_RESET_PULSE_US};
unsigned long last_good_message_received=0;

char mqtt_server[512] = "192.168.178.29";
char mqtt_port[6] = "1883";
char mqtt_user[64] = "";
char mqtt_pass[64] = "";

WiFiClient espClient;
PubSubClient client(espClient);
boolean shouldSaveConfig=false;

unsigned int channel_counter=0;
unsigned long state_change_time=micros();
boolean ppm_on=true;

void ICACHE_RAM_ATTR onTimerISR(){
    
  if(ppm_on){
    ppm_on=false;
    // output_pin off
    digitalWrite(PPM_PIN, LOW);
    channel_counter++;
    if(channel_counter>NUM_CHANNELS){
      channel_counter=0;
    }    
    timer1_write(5*((channel_output[channel_counter])-PPM_CHANNEL_SEPARATOR_US));//5 ticks per us
  }else{
    ppm_on=true;
    // output_pin on
    digitalWrite(PPM_PIN, HIGH);
    timer1_write(5*PPM_CHANNEL_SEPARATOR_US);//5 ticks per us
  }

}

void saveConfigCallback () {
  Serial.println("Should save config");
  shouldSaveConfig = true;
}

void pubsubCallback(char* topic, byte* payload, unsigned int length) {
  //Serial.print("Message arrived [");
  //Serial.print(topic);
  //Serial.print("] ");
  String topicString = topic;
  char payloadStr[length+1];
  for (int i = 0; i < length; i++) {
    //Serial.print((char)payload[i]);
    payloadStr[i]=(char)payload[i];
  }
  //Serial.println();

  if(topicString.equals("gamepad/axis/mix1")){
    channel_output[1]=atoi(payloadStr);
    last_good_message_received=millis();
  }

  if(topicString.equals("gamepad/axis/mix2")){
    channel_output[2]=atoi(payloadStr);
    last_good_message_received=millis();
  }  
}

void setup() {
  // put your setup code here, to run once:
  pinMode(PPM_PIN, OUTPUT);
  digitalWrite(PPM_PIN, HIGH);

  // put your setup code here, to run once:
  Serial.begin(115200);
  Serial.println();

  //clean FS for testing 
//  SPIFFS.format();

  //read configuration from FS json
  Serial.println("mounting FS...");

  if (SPIFFS.begin()) {
    Serial.println("mounted file system");
    if (SPIFFS.exists("/config.json")) {
      //file exists, reading and loading
      Serial.println("reading config file");
      File configFile = SPIFFS.open("/config.json", "r");
      if (configFile) {
        Serial.println("opened config file");
        size_t size = configFile.size();
        // Allocate a buffer to store contents of the file.
        std::unique_ptr<char[]> buf(new char[size]);

        configFile.readBytes(buf.get(), size);
        DynamicJsonBuffer jsonBuffer;
        JsonObject& json = jsonBuffer.parseObject(buf.get());
        json.printTo(Serial);
        if (json.success()) {
          Serial.println("\nparsed json");
          strcpy(mqtt_server, json["mqtt_server"]);
          strcpy(mqtt_port, json["mqtt_port"]);
          strcpy(mqtt_user, json["mqtt_user"]);
          strcpy(mqtt_pass, json["mqtt_pass"]);

        } else {
          Serial.println("failed to load json config");
        }
      }
    }
  } else {
    Serial.println("failed to mount FS");
  }
  //end read

  // The extra parameters to be configured (can be either global or just in the setup)
  // After connecting, parameter.getValue() will get you the configured value
  // id/name placeholder/prompt default length
  WiFiManagerParameter custom_mqtt_server("server", "mqtt server", mqtt_server, 40);
  WiFiManagerParameter custom_mqtt_port("port", "mqtt port", mqtt_port, 6);
  WiFiManagerParameter custom_mqtt_user("user", "mqtt user", mqtt_user, 20);
  WiFiManagerParameter custom_mqtt_pass("pass", "mqtt pass", mqtt_pass, 20);

  //WiFiManager
  //Local intialization. Once its business is done, there is no need to keep it around
  WiFiManager wifiManager;

// Reset Wifi settings for testing  
//  wifiManager.resetSettings();

  //set config save notify callback
  wifiManager.setSaveConfigCallback(saveConfigCallback);

  //set static ip
//  wifiManager.setSTAStaticIPConfig(IPAddress(10,0,1,99), IPAddress(10,0,1,1), IPAddress(255,255,255,0));
  
  //add all your parameters here
  wifiManager.addParameter(&custom_mqtt_server);
  wifiManager.addParameter(&custom_mqtt_port);
  wifiManager.addParameter(&custom_mqtt_user);
  wifiManager.addParameter(&custom_mqtt_pass);

  //reset settings - for testing
  //wifiManager.resetSettings();

  //set minimum quality of signal so it ignores AP's under that quality
  //defaults to 8%
  //wifiManager.setMinimumSignalQuality();
  
  //sets timeout until configuration portal gets turned off
  //useful to make it all retry or go to sleep
  //in seconds
  //wifiManager.setTimeout(120);

  //fetches ssid and pass and tries to connect
  //if it does not connect it starts an access point with the specified name
  //here  "AutoConnectAP"
  //and goes into a blocking loop awaiting configuration
  if (!wifiManager.autoConnect("AutoConnectAP", "password")) {
    Serial.println("failed to connect and hit timeout");
    delay(3000);
    //reset and try again, or maybe put it to deep sleep
    ESP.reset();
    delay(5000);
  }

  //if you get here you have connected to the WiFi
  Serial.println("connected...yeey :)");

  //read updated parameters
  strcpy(mqtt_server, custom_mqtt_server.getValue());
  strcpy(mqtt_port, custom_mqtt_port.getValue());
  strcpy(mqtt_user, custom_mqtt_user.getValue());
  strcpy(mqtt_pass, custom_mqtt_pass.getValue());
 // strcpy(blynk_token, custom_blynk_token.getValue());

  //save the custom parameters to FS
  if (shouldSaveConfig) {
    Serial.println("saving config");
    DynamicJsonBuffer jsonBuffer;
    JsonObject& json = jsonBuffer.createObject();
    json["mqtt_server"] = mqtt_server;
    json["mqtt_port"] = mqtt_port;
    json["mqtt_user"] = mqtt_user;
    json["mqtt_pass"] = mqtt_pass;

    File configFile = SPIFFS.open("/config.json", "w");
    if (!configFile) {
      Serial.println("failed to open config file for writing");
    }

    json.printTo(Serial);
    json.printTo(configFile);
    configFile.close();
    //end save
  }

  Serial.println("local ip");
  Serial.println(WiFi.localIP());
//  client.setServer(mqtt_server, 12025);
 // const uint16_t mqtt_port_x = 12025; 
  client.setServer(mqtt_server, atoi(mqtt_port));
  client.setCallback(pubsubCallback);

  timer1_attachInterrupt(onTimerISR);
  timer1_enable(TIM_DIV16, TIM_EDGE, TIM_SINGLE);
  timer1_write(5*1000); //1000 us  
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    // If you do not want to use a username and password, change next line to
    // if (client.connect("ESP8266Client")) {
    if (client.connect("ESP8266Client", mqtt_user, mqtt_pass)) {
      Serial.println("connected");
      client.subscribe("gamepad/axis/#");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void loop() {

  if (!client.connected()) {
    reconnect();
  }
  client.loop();
  if(last_good_message_received+FAILSAFE_TIME_MS < millis()){
    // failsafe
    for (int i=0; i<NUM_CHANNELS; i++){
      channel_output[i]=failsafe_output[i];
    }
  }  
}